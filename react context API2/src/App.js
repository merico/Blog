import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Show from './component/Show';
import {AppContext, ConTextStates} from './context/ThemeContext'
import {toggleTheme} from './context/handlers/ThemeHandler';
import Button from './component/Button'

class App extends Component {
    constructor(props) {
        super(props);

        //可以认为是项目的全局变量
        this.state = {
            ...ConTextStates
        };

        //将自定义的方法绑定到App中
        this.toggleTheme = toggleTheme.bind(this);
    }

    render() {
        return (
            <AppContext.Provider value={{...this.state, toggleTheme: this.toggleTheme}}>
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo"/>
                        <h1 className="App-title">Welcome to React</h1>
                    </header>
                    <div className="App-intro">
                        <Show/>
                        <Button/>
                    </div>
                </div>
            </AppContext.Provider>
        )
    }
}

///////////////////// 博客中的简单实例 /////////////////////

// import React, {Component} from 'react';
// import logo from './logo.svg';
// import './App.css';
// import ContextDemo from "./SimpleDemo/ContextDemo";
//
// class App extends Component {
//
//     render() {
//         return (
//             <div className="App">
//                 <header className="App-header">
//                     <img src={logo} className="App-logo" alt="logo"/>
//                     <h1 className="App-title">Welcome to React</h1>
//                 </header>
//                 <div className="App-intro">
//                     <ContextDemo/>
//                 </div>
//             </div>
//         )
//     }
// }

export default App;
