import React, {Component} from 'react'
import {AppContext} from '../context/ThemeContext'

class Button extends Component {
    toggleHandler = (context) => {
        if (context.background === 'yellow') {
            context.toggleTheme('blue');
        } else {
            context.toggleTheme('yellow');
        }
    };

    render() {
        return (
            <AppContext.Consumer>
                {
                    (context) => (
                        <button onClick={() => this.toggleHandler(context)}>Toggle</button>
                    )
                }
            </AppContext.Consumer>
        )
    }
}

export default Button;