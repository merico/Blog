import React, {Component} from 'react';
import {AppContext} from '../context/ThemeContext'

class Show extends Component {
    render() {
        return (
            <AppContext.Consumer>
                {
                    (context) => (
                        <div>
                            <h1 style={{background: context.background}}>Hello World</h1>
                        </div>
                    )
                }
            </AppContext.Consumer>
        )
    }
}

export default Show;