import React, {Component} from 'react';
import intl from 'react-intl-universal';

class I18N extends Component {

    changeLanguage = (language) => {
        window.location.search = `?lang=${language}`;
    };

    render() {
        return (
            <div>
                <button onClick={()=>{this.changeLanguage('zh-CN')}}>中文</button>
                <button onClick={()=>{this.changeLanguage('en-US')}}>English</button>

                <div>
                    <hr/>
                    <p>获取普通字符串</p>
                    <p>{intl.get('name')}</p>
                    <p>{intl.get('age')}</p>
                    <p>{intl.get('address')}</p>
                    <p>{intl.get('red')}</p>

                    <hr/>
                    <p>获取html形式的内容</p>
                    <p>{intl.getHTML('red')}</p>
                    <p>{intl.getHTML('h1')}</p>

                    <hr/>
                    <p>默认值</p>
                    <p>{intl.get('not-exist-key').defaultMessage('default message')}</p>
                    <p>{intl.get('not-exist-key').d('default message')}</p> {/*d()是defaultMessage()的缩写*/}
                    <p>{intl.getHTML('not-exist-key').d(<div>hello</div>)}</p>

                    <hr/>
                    {/*json某个value如果涉及到变量那么用{}包一下内部写上变量名，获取的时候第二个参数为变量值的键值对对象*/}
                    <p>带参数</p>
                    <p>{intl.get('me', {'me': 'Mario'})}</p>

                    <hr/>
                    {/*
                        The syntax is {name, type, format}. Here is description:
                          1 name is the variable name in the message. In this case, it's price.
                          2 type is the type of value such as number, date, and time.
                          3 format is optional, and is additional information for the displaying format of the value. In this case, it's USD.
                    */}
                    <p>处理货币格式化</p>
                    <p>{intl.get('price', {'price': 1000})}</p>

                    <hr/>
                    {/*
                        If type is date, format has the following values:
                            1 short: shows date as shortest as possible
                            2 medium: shows short textual representation of the month
                            3 long: shows long textual representation of the month
                            4 full: shows dates with the most detail
                    */}
                    <p>处理日期</p>
                    <p>{intl.get('date',{'date':new Date()})}</p>

                    <hr/>
                    {/*
                        if type is time, format has the following values:
                            1 short: shows times with hours and minutes
                            2 medium: shows times with hours, minutes, and seconds
                            3 long: shows times with hours, minutes, seconds, and timezone
                    */}
                    <p>处理时间</p>
                    <p>{intl.get('time',{'time':new Date()})}</p>
                </div>
            </div>
        )
    }
}

export default I18N;