import React, {Component} from 'react';
import intl from 'react-intl-universal';
import I18N from './International';
import http from "axios";
import logo from './logo.svg';
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initDone: false
        }
    }

    componentDidMount() {
        this.loadLocales();
    }

    loadLocales() {
        const _self = this;
        let currentLocale = intl.determineLocale({  //如果cookie和url中均没有相关参数，那么以浏览器语言为准
            urlLocaleKey: "lang",
            cookieLocaleKey: "lang"
        });

        http
            .get(`locales/${currentLocale}.json`)   //理解为按需加载并且locales文件夹需要放在public文件下供http访问
            .then(res => {
                return intl.init({
                    currentLocale,
                    locales: {
                        [currentLocale]: res.data //如果key是变量，那么需要用[]包一下
                    }
                });
            })
            .then(() => {
                _self.setState({initDone: true});
            });
    }

    render() {
        return (
            this.state.initDone &&
            <div className="App">
                <header>
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Welcome to React i18n</h1>
                </header>
                <div className="App-intro">
                    <I18N/>
                </div>
            </div>
        );
    }
}

export default App;
