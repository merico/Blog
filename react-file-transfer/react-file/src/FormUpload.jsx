import React, {Component} from 'react'

class FormUpload extends Component {
    submit = (e) => {
        e.preventDefault();
        let formData = new FormData(e.target);
        fetch('http://127.0.0.1:3001/file/upload', {
            method: 'POST',
            body: formData //自动将input:file的name属性与文件对象组合成键值对
        }).then(response => console.log(response))
    };

    render() {
        return (
            <div>
                <form onSubmit={this.submit}>
                    <input type="file" name='file'/>
                    <input type="submit" value="上传"/>
                </form>
            </div>
        )
    }
}

export default FormUpload;


/*
* 提交后不刷新不跳转的解决方案
* 1、使用iframe来实现   https://blog.csdn.net/stpeace/article/details/50757929
* 2、Fetch + FromData，原理是阻止form表单本身的提交行为并提取出form表单的数据(用FormData包装一次)通过Fetch或者Ajax或者Axios进行提交
*/