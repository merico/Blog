import React, {Component} from 'react'

class FormDownload extends Component {
    render() {
        return (
            <form method="get" action="http://127.0.0.1:3001/file/download">
                <button type="submit">Download!</button>
            </form>
        )
    }
}

export default FormDownload;


/*
* 没啥原理，最大的优点是不用考虑跨域
*/