import React, {Component} from 'react'

class FormUploadOnly extends Component {
    render() {
        return (
            <div>
                <form action="http://127.0.0.1:3001/file/upload" method="post" enctype="multipart/form-data">
                    <input type="file" name='file'/>
                    <input type="submit" value="上传"/>
                </form>
            </div>
        )
    }
}

export default FormUploadOnly;