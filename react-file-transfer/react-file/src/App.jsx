import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

import FormDownload from "./FormDownload";
import FetchDownload from "./FetchDownload";
import FormUpload from "./FormUpload";
import FetchUpload from "./FetchUpload";
import FormUploadOnly from './FormUploadOnly'

class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <div>
                    <FetchUpload/>
                </div>
            </div>
        );
    }
}

export default App;
