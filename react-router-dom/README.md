This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Table of Contents

- [Updating to New Releases](#updating-to-new-releases)
- [Sending Feedback](#sending-feedback)
- [Folder Structure](#folder-structure)
- [Available Scripts](#available-scripts)
  - [npm start](#npm-start)
  - [npm test](#npm-test)
  - [npm run build](#npm-run-build)
  - [npm run eject](#npm-run-eject)


## Notifications
- When you use a basename, the basename should not be included in the route path. [引自GitHub]
- 嵌套路由
  - 子路由不需要Router
  - 将父路由的根目录传给子路由
- 默认路由
  - 一般使用 <Redirect exact from="/" to="/xxx"/> 来实现
- 相同根目录的路由可以在 <Route />组件中进行封装; 详情见 'NestingRouter.js' 中的 '/Fronted'
- 参数处理
  - <Route path={`${BASE}/name/:name`} component={Name}/> 在对应组件的 this.props.match.params 中取到对应参数  name
  - <Route path={`${BASE}/check/:check(true|false)`} component={Check}/> 对参数进行限制，在这里只能是 true 或者 false