import React, { Component } from 'react';
import { Route, NavLink, Switch, Redirect, withRouter } from "react-router-dom";
import Style from './style/Authentication.css';

class Authentication extends Component {
    render() {
        const URL = this.props.url;
        const PATH = this.props.path;
        return (
            <div className={Style["Authentication-Container"]}>
                <div className={Style["Container-body"]}>
                    <nav className={Style["Authentication-nav-list"]}>
                        <ul>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/Home`}>Authentication</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/NoAuth`}>noAuth</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/Auth`}>Auth</NavLink></li>
                        </ul>
                    </nav>
                    <div className={Style.content}>
                        <Switch>
                            <Redirect exact from={PATH} to={`${PATH}/Home`} />
                            <Route path={`${PATH}/Home`} component={HomePage} />

                            <Route path={`${PATH}/NoAuth`} component={NoAuth} />
                            <Route exact path={`${PATH}/Auth`} component={AuthRender} />
                            <Route path={`${PATH}/Auth/Login`} component={Login} />
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }
}

const AuthTool = {
    isAuthenticated: false,
    login(cb) {
        this.isAuthenticated = true;
        cb();
    },
    logout(cb) {
        this.isAuthenticated = false;
        cb();
    }
};


class HomePage extends Component {
    render() {
        return (
            <div>
                <a href='https://reacttraining.com/react-router/web/example/auth-workflow'>
                    <h1>React-Router-Dom_Authentication</h1>
                </a>
            </div>
        )
    }
}

class NoAuth extends Component {
    render() {
        return (
            <h1>JavaScript</h1>
        )
    }
}


//根据验证状态正常显示页面还是转向登录页面
const AuthRender = ({ match }) => {
    if (AuthTool.isAuthenticated) {
        return <Authenticated />
    } else {
        return <Redirect to={`${match.path}/Login`} />  //   auth/Auth/Login
    }
};

class Authenticated extends Component {
    render() {
        return (
            <div>
                <h1>Login Successfully</h1>
                <AuthButton />
            </div>
        )
    }
}

class Login extends Component {
    render() {
        return (
            <div>
                <AuthButton />
            </div>
        )
    }
}

/**
 * 负责登录登出的按钮
 *
 * 1 更改登录状态
 * 2 页面跳转
 */
const AuthButton = withRouter(
    ({ history }) =>
        AuthTool.isAuthenticated ? (
            <div>
                Welcome!{" "}
                <button
                    onClick={() => {
                        AuthTool.logout(() => history.push("/auth/Auth/Login"));  //登出后跳转到登录页面
                    }}
                >
                    Logout
                </button>
            </div>
        ) : (
                <div>
                    You are not logged in!
                <button
                        onClick={() => {
                            AuthTool.login(() => history.push("/auth/Auth")); //登出后跳转到实际页面
                        }}
                    >
                        Login
                </button>
                </div>
            )
);


export default Authentication;
