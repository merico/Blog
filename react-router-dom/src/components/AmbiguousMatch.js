import React, { Component } from 'react';
import { Route, NavLink, Switch, Redirect } from "react-router-dom";
import Style from './style/AmbiguousMatch.css';

class AmbiguousMatch extends Component {
    render() {
        const URL = this.props.url;
        const PATH = this.props.path;
        return (
            <div className={Style["Ambiguous-Container"]}>
                <div className={Style["Container-body"]}>
                    <nav className={Style["Ambiguous-nav-list"]}>
                        <ul>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/Home`}>AmbiguousMatch</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/JavaScript`}>/JavaScript</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/Fronted`}>/Fronted</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/Mario`}>/Mario</NavLink></li>
                        </ul>
                    </nav>
                    <div className={Style.content}>
                        <Switch>
                            <Redirect exact from={PATH} to={`${PATH}/Home`} />
                            <Route path={`${PATH}/Home`} component={HomePage} />

                            <Route path={`${PATH}/JavaScript`} component={Language} />
                            <Route path={`${PATH}/Fronted`} component={Work} />
                            <Route path={`${PATH}/:name`} component={Name} />
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }
}

class HomePage extends Component {
    render() {
        return (
            <div>
                <a href='https://reacttraining.com/react-router/web/example/ambiguous-matches'>
                    <h1>React-Router-Dom_Ambiguous_Match</h1>
                </a>
            </div>
        )
    }
}

class Language extends Component {
    render() {
        return (
            <h1>JavaScript</h1>
        )
    }
}

class Name extends Component {
    render() {
        const name = this.props.match.params.name;
        return (
            <h1>Name: {name}</h1>
        )
    }
}

class Work extends Component {
    render() {
        return (
            <h1>Fronted</h1>
        )
    }
}

export default AmbiguousMatch;
