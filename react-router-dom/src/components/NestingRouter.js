import React, { Component } from 'react';
import { Route, Switch, Redirect, NavLink } from "react-router-dom";
import Style from './style/NestingRouter.css';

class NestingRouter extends Component {
    render() {
        const URL = this.props.url;
        const PATH = this.props.path;
        return (
            <div className={Style["Nesting-Container"]}>
                <div className={Style["Container-body"]}>
                    <nav className={Style["Nesting-nav-list"]}>
                        <ul>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/home`}>Nesting Router</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/JavaScript`}>/JavaScript</NavLink></li>

                            <li><NavLink exact activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/Fronted`}>/Fronted</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/Fronted/WebPack`}>/Fronted/WebPack</NavLink></li>
                        </ul>
                    </nav>
                    <div className={Style.content}>
                        <Redirect exact from={PATH} to={`${PATH}/Home`} />
                        <Route path={`${PATH}/Home`} component={HomePage} />

                        <Route path={`${PATH}/JavaScript`} component={Language} />
                        <Route path={`${PATH}/Fronted`} component={() => <Technology path={PATH} />} />
                    </div>
                </div>
            </div>
        );
    }
}

class HomePage extends Component {
    render() {
        return (
            <div>其实这个项目的主页面就是用嵌套路由的方式构建的</div>
        )
    }
}

class Language extends Component {
    render() {
        return (
            <h1>JavaScript</h1>
        )
    }
}

class Technology extends Component {
    render() {
        const PATH = this.props.path;
        return (
            <Switch>
                <Route path={`${PATH}/Fronted/:name`} component={Name} />
                <Route path={`${PATH}/Fronted`} component={Fronted} />
            </Switch>
        )
    }
}

class Name extends Component {
    render() {
        const name = this.props.match.params.name;
        return (
            <h1>Name: {name}</h1>
        )
    }
}

class Fronted extends Component {
    render() {
        return (
            <h1>Fronted</h1>
        )
    }
}

export default NestingRouter;
