import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink, Redirect, Switch } from "react-router-dom";
import Basic from './Basic';
import RouterWithParameters from './RouterWithParameters';
import AmbiguousMatch from './AmbiguousMatch';
import NestingRouter from './NestingRouter';
import Authentication from './Authentication';
import AppStyle from './style/App.css';

class App extends Component {
    render() {
        return (
            <div className={AppStyle["App-Container"]}>
                <header className={AppStyle["Container-header"]}>
                    <h1>Welcome to React Router Demo</h1>
                </header>
                <Router>
                    <div className={AppStyle["Container-body"]}>
                        <nav className={AppStyle["App-nav-list"]}>
                            <ul>
                                <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to="/basic">基础路由</NavLink></li>
                                <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to="/param">带参路由</NavLink></li>
                                <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to="/nesting">嵌套路由</NavLink></li>
                                <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to="/ambiguous">暧昧匹配</NavLink></li>
                                <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to="/auth">权限路由</NavLink></li>
                                <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to="/404">404</NavLink></li>
                            </ul>
                        </nav>
                        <div className={AppStyle.content}>
                            <Switch>
                                <Redirect exact from="/" to="/basic" />
                                <Route path="/basic" component={renderBasicRouter} />

                                <Route path="/param" component={RouterWithParameters} />
                                <Route path="/ambiguous" component={renderAmbiguousRouter} />
                                <Route path="/nesting" component={renderNestingRouter} />
                                <Route path="/auth" component={authenticationRouter} />
                                <Route component={NoMatch} />
                            </Switch>
                        </div>
                    </div>
                </Router>
            </div>
        );
    }
}

const NoMatch = ({ location }) => (
    <div>
        <h3>
            No match for <code>{location.pathname}</code>
        </h3>
    </div>
);

const renderBasicRouter = ({ match }) => <Basic url={match.url} path={match.path} />;
// const renderParamRouter = ({ match }) => <RouterWithParameters url={match.url} path={match.path} />;
const renderAmbiguousRouter = ({ match }) => <AmbiguousMatch url={match.url} path={match.path} />;
const renderNestingRouter = ({ match }) => <NestingRouter url={match.url} path={match.path} />;
const authenticationRouter = ({ match }) => <Authentication url={match.url} path={match.path} />;

export default App;
