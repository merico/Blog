import React, { Component } from 'react';
import { Route, NavLink, Redirect, Switch } from "react-router-dom";
import Style from './style/RouterWithParameters.css';

class RouterWithParameters extends Component {
    render() {
        const URL = this.props.match.url;
        const PATH = this.props.match.path;
        return (
            <div className={Style["Param-Container"]}>
                <div className={Style["Container-body"]}>
                    <nav className={Style["Params-nav-list"]}>
                        <ul>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/home`}>Router-Param</NavLink></li>
                            <li><NavLink exact activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/name`}>/name</NavLink></li>
                            <li><NavLink exact activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/name/Mario`}>/name/Mario</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/check/true`}>/check/true</NavLink></li>
                        </ul>
                    </nav>
                    <div className={Style.content}>
                        <Switch>
                            <Redirect exact from={PATH} to={`${PATH}/home`} />
                            <Route exact path={`${PATH}/home`} component={HomePage} />

                            <Route exact path={`${PATH}/name/:name`} component={Name} />
                            <Route exact path={`${PATH}/name`} component={Name1} />
                            <Route exact path={`${PATH}/check/:check(true|false)`} component={Check} />
                            <Route component={NoMatch} />
                        </Switch>
                    </div>
                </div>
            </div>
        )
    }
}

const NoMatch = ({ location }) => (
    <div>
        <h3>
            No match for <code>{location.pathname}</code>
        </h3>
    </div>
);

class HomePage extends Component {
    render() {
        return (
            <div>
                <a href='https://reacttraining.com/react-router/web/example/url-params'>
                    <h1>React-Router-Dom_Parameters</h1>
                </a>
            </div>
        )
    }
}

class Name1 extends Component {
    render() {
        return (
            <div>
                NameNameNameName
            </div>
        )
    }
}

class Name extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ''
        }
    }

    componentDidMount() {
        this.setState({
            name: this.props.match.params.name
        });
    }

    render() {
        return (
            <h1>Name: {this.state.name}</h1>
        )
    }
}

class Check extends Component {
    constructor(props) {
        super(props);
        this.state = {
            check: ''
        }
    }

    componentDidMount() {
        this.setState({
            check: this.props.match.params.check
        });
    }

    render() {
        return (
            <h1>Check: {this.state.check}</h1>
        )
    }
}

export default RouterWithParameters;