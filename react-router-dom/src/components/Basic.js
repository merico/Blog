import React, { Component } from 'react';
import {Route, NavLink, Switch, Redirect } from "react-router-dom";
import BasicStyle from './style/Basic.css';

class Basic extends Component {
    render() {
        const URL = this.props.url;
        const PATH = this.props.path;
        return (
            <div className={BasicStyle["Basic-Container"]}>
                <div className={BasicStyle["Container-body"]}>
                    <nav className={BasicStyle["Basic-nav-list"]}>
                        <ul>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/Home`}>Router-Basic</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/JavaScript`}>/JavaScript</NavLink></li>
                            <li><NavLink activeStyle={{ fontWeight: 'bold', color: 'red' }} to={`${URL}/Fronted`}>/Fronted</NavLink></li>
                        </ul>
                    </nav>
                    <div className={BasicStyle.content}>
                        <Switch>
                            <Redirect exact from={PATH} to={`${PATH}/Home`} />
                            <Route path={`${PATH}/Home`} component={HomePage} />

                            <Route path={`${PATH}/JavaScript`} component={Language} />
                            <Route path={`${PATH}/Fronted`} component={Work} />
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }
}

class HomePage extends Component {
    render() {
        return (
            <div>
                <a href='https://reacttraining.com/react-router/web/example/basic'><h1>React-Router-Dom_Basic</h1></a>
            </div>
        )
    }
}

class Language extends Component {
    render() {
        return (
            <h1>JavaScript</h1>
        )
    }
}

class Work extends Component {
    render() {
        return (
            <h1>Fronted</h1>
        )
    }
}

export default Basic;
