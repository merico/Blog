import React, {Component} from 'react';

class Demo extends Component {
    constructor(props) {
        super(props);
    }

    handler = () => {
        alert('Click!')
    };

    render() {
        return (
            <div onClick={this.handler}>
                点我
            </div>
        );
    }
}

export default Demo;