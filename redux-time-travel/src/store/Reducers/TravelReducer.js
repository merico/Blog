import {ADD, UNDO, REDO} from '../Types';


export default function TravelReducer(state = {}, action) {
    switch (action.type) {
        case ADD:
            let payloadContent = action.payload['list'];
            let archive = state['list'].slice();
            let currentIndex_ADD = state['currentIndex'];
            archive.push(payloadContent);
            return {...state, ...{'list': archive, 'currentIndex': currentIndex_ADD + 1}};
        case UNDO:
            let currentIndex_UNDO = state['currentIndex'];
            return {...state, ...{'currentIndex': currentIndex_UNDO - 1}};
        case REDO:
            let currentIndex_REDO = state['currentIndex'];
            return {...state, ...{'currentIndex': currentIndex_REDO + 1}};
        default:
            return state
    }
}