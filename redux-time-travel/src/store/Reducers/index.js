import {combineReducers} from 'redux';
import TravelReducer from './TravelReducer';


const rootReducer = combineReducers({
    travel: TravelReducer
});

export default rootReducer;