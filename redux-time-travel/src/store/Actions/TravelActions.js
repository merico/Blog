import {ADD, UNDO, REDO} from '../Types';


export function Add(list) {
    return {
        type: ADD,
        payload: {'list': list}
    }
}

export function Undo() {
    return {
        type: UNDO,
    }
}

export function Redo() {
    return {
        type: REDO,
    }
}