import React, {Component} from 'react';
import {Provider} from 'react-redux';
import './App.css';
import Addition from './components/Addition'
import configureStore from './redux/store'

const store = configureStore({});

class App extends Component {
    clickHandler = () => {
        // this.refs.addition.addHandler();
        this.refs.addition.getWrappedInstance().addHandler();
    };

    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <Addition ref='addition'/>
                    <button onClick={this.clickHandler}>加加加</button>
                </div>
            </Provider>
        );
    }
}

export default App;


