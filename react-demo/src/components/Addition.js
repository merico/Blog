import React, {Component} from 'react';
import {connect} from 'react-redux'

class Addition extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }

    addHandler = () => {
        this.setState({
            value: this.state.value + 1
        })
    };

    render() {
        return (
            <div>
                <p>{this.state.value}</p>
            </div>
        )
    }
}

// export default Addition

function mapStateToProps(state) {return {}}
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps, null, {withRef: true})(Addition)