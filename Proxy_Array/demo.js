let vm = {
    list: []
}

let proxy = new Proxy(vm, {
    set(target, prop, value) {
        console.log('Setting');
        Reflect.set(target, prop, value);
        return true
    }
})

proxy.list.push(1)
// proxy.list = [1, 2, 3, 4];
console.log(Array.from(proxy.list))